from __future__ import annotations

import os
from dataclasses import dataclass
from typing import Literal, Any

from mollie.api.client import Client


@dataclass
class PaymentResponse:
    checkout_url: str
    status: Literal['confirmed', 'refused', 'pending']
    id: str


def handle_payment(payment_submission: dict[str, Any]) -> PaymentResponse:
    client = get_mollie_client()
    payment = client.payments.create(payment_submission)
    return PaymentResponse(checkout_url=payment.checkout_url, status=payment.status, id=payment['id'])


def fetch_payment_status(payment_id: str) -> str:
    client = get_mollie_client()
    payment = client.payments.get(payment_id)
    return payment.status


def get_mollie_client() -> Client:
    api_key = os.environ['API_KEY']
    mollie_client = Client()
    mollie_client.set_api_key(api_key)
    return mollie_client


def get_payment_methods() -> dict[Literal['creditcard', 'ideal'], Any]:
    mollie_client = get_mollie_client()
    issuers = mollie_client.methods.get('ideal', include='issuers').issuers
    return {
        'creditcard': {},
        'ideal': issuers
    }
