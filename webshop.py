from __future__ import annotations

import os
from typing import Any

from flask import Flask, render_template, request, redirect, url_for, current_app

from payment import handle_payment, get_payment_methods

app = Flask(__name__)


@app.route('/', methods=['GET'])
def checkout():
    payment_methods = get_payment_methods()
    return render_template(
        'index.html', payment_methods=payment_methods,
    )


@app.route('/', methods=['POST'])
def submit_payment():
    payment_submission = parse_submit_payment_request(request.form)

    response = handle_payment(payment_submission)
    current_app.logger.info(str(response))

    return redirect(response.checkout_url)


def parse_submit_payment_request(submitted_form) -> dict[str, Any]:
    payment_method = submitted_form['paymentMethod']

    confirmation_url = url_for(checkout.__name__, _external=True)
    interfacing_url = os.environ['INTERFACING_URL']

    payload = {
        'amount': {
            'currency': 'EUR',
            'value': '10.00'
        },
        'description': 'My first API payment',
        'redirectUrl': confirmation_url,
        'webhookUrl': interfacing_url,
        'method': payment_method,
    }
    if payment_method == 'ideal':
        payload['issuer'] = submitted_form['issuer']

    return payload
