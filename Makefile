image:
	docker build -t mollie-components .

run-server: image
	docker run \
	  -p 5000:5000 \
	  --env-file=$(shell pwd)/.webshop-env \
	  -v $(shell pwd):/srv \
	  mollie-components \
	  flask run --host 0.0.0.0

run-interfacing: image
	docker run \
	  -p 5003:5003 \
	  --env-file=$(shell pwd)/.interfacing-env \
	  -v $(shell pwd):/srv \
	  mollie-components \
	  flask run --host 0.0.0.0 --port 5003
