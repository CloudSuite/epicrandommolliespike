from __future__ import annotations

from flask import Flask, request

from payment import fetch_payment_status

app = Flask(__name__)


@app.route('/', methods=['GET', 'POST'])
def checkout():
    payment_id = request.form.get('id')
    app.logger.info(fetch_payment_status(payment_id))

    return 'kthanks', 200
